using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Pokedex.Models{
    [Table("pokemon")]
    public class Pokemon{
        [Column("id")]
        public long id { get; set;}
        [Column("name")]
        public string name { get; set;}
        [Column("height")]
        public int height { get; set;}
        [Column("weight")]
        public int weight { get; set;}

        public Pokemon(string name, int height, int weight){
            this.name = name;
            this.height = height;
            this.weight = weight;
        }
        public Pokemon(string name){
            this.name = name;
        }
        public Pokemon(){

        }
    }
}