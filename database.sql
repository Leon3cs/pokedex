--------------------
--    drop all    --
--------------------

drop table if exists pokemon;
drop sequence if exists pkmn_id_seq;

--------------------
--    Pokemons    --
--------------------

create sequence pkmn_id_seq increment by 1 minvalue 1 maxvalue 9223372036854775807 start with 1 cache 1;

create table pokemon (
    id bigint default nextval('pkmn_id_seq'),
    name character varying(50) ,
    height int,
    weight int
);