using Microsoft.AspNetCore.Mvc; 
using System.Collections.Generic; 
using System.Data.Common;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;
using Pokedex.Models;
using RestSharp;
using Newtonsoft.Json;
using RestSharp.Serialization.Json;
using System;
using Microsoft.EntityFrameworkCore;

namespace Pokedex.Controllers{ 

    [Route("api/[controller]")]     
    [ApiController] 
    public class PokemonsController : ControllerBase {

        public readonly string baseApiUrl = "https://pokeapi.co/api/v2/pokemon/";

        private readonly PokemonContext _context;

        public PokemonsController (PokemonContext context){
            this._context = context;
        }

        [HttpGet]
        public ActionResult<List<Pokemon>> getPokemons(){
            return _context.Pokemons.ToList();
        }

        [HttpPost("{pokemonName}")]
        public Pokemon getPokemon(string pokemonName){
            var client = new RestClient(baseApiUrl);
            var request = new RestRequest("{pokemonName}", Method.GET);
            request.AddUrlSegment("pokemonName", pokemonName);
            var response = client.Execute(request);
            if(!response.Content.Equals("Not Found")){
                var dSerialize = new JsonDeserializer();
                var output = dSerialize.Deserialize<Dictionary<string, string>>(response);
                
                string pkmnName = output["name"];
                int pkmnHeight = int.Parse(output["height"]);
                int pkmnWeight = int.Parse(output["weight"]);
                
                Pokemon pkmn = new Pokemon(pkmnName, pkmnHeight, pkmnWeight);

                _context.Pokemons.Add(pkmn);
                int result = _context.SaveChanges();

                return pkmn;
            }else{
                return null;
            }
        }

        [HttpPut("{id}")]
        public Pokemon updatePokemon(int id, Pokemon item){
            Pokemon pkmn = _context.Pokemons.Where(p => p.id == id).Single();
            pkmn.name = item.name;
            pkmn.height = item.height;
            pkmn.weight = item.weight;
            _context.Entry(pkmn).State = EntityState.Modified;
            _context.SaveChanges();
            List<Pokemon> list = _context.Pokemons.ToList();
            return pkmn;
        }

        [HttpDelete("{id}")]
        public void deletePokemon(int id){
            Pokemon pkmn = _context.Pokemons.Where(p => p.id == id).Single();
            _context.Pokemons.Remove(pkmn);
            _context.SaveChanges();
        }
    }
}